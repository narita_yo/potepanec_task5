require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe '#show' do
    before do
      get :show, params: { id: product.id }
    end

    let!(:taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    it "レスポンスが正しい" do
      expect(response).to be_successful
    end

    it "テンプレートが正しい" do
      expect(response).to render_template :show
    end

    it "インスタンスが正しい" do
      expect(assigns(:product)).to eq(product)
    end

    it "正しい関連商品を取得している" do
      # 4つ以上表示されていないか（limit(RELATED_PRODUCTS_LIMITが適用されているか）
      expect(assigns(:related_products)).to match_array related_products[0..3]
    end
  end
end
