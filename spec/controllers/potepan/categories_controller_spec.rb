require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "#show" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:product) { create(:product, taxons: [taxon]) }

    before { get :show, params: { id: taxon.id } }

    it "レスポンスが正しい" do
      expect(response).to be_successful
    end

    it "テンプレートが正しい" do
      expect(response).to render_template :show
    end

    it "正しく@taxonが渡されている" do
      expect(assigns(:taxon)).to eq(taxon)
    end

    it "正しく@taxonomiesが渡されている" do
      expect(assigns(:taxonomies)).to match_array(taxonomy)
    end

    it "正しく@productsが渡されている" do
      expect(assigns(:products)).to match_array(product)
    end
  end
end
